New Joiners
=========

In preperation for a more stream lined approach to managing users

steps:
  create ssh private keypair
  create freeipa user
    https://docs.ansible.com/ansible/2.5/modules/ipa_user_module.html
  make keypair accessible to the user via minio with inital password
  add user to guacamole 
    https://github.com/hijak/guacamole-rest-test


Requirements
------------

docker
freeipa
have your minio server defined in inventory

Role Variables
--------------
Normal vars:
```
ipa_host: 
ipa_user: 
smtp-server: 
guacamole_url:
jira_user:
```

Vault vars:
```
vault_guacamole_admin_password:
vault_slack_api_token:
vault_jira_password:
vault_ipa_pass:
```
`firstname` `surname` and `username` Are provided as prompts when running the playbook. Slack token must be legacy 

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Designed to delegate minio stuff to the minio server

```
- hosts: localhost
  roles:
    - newjoiner
  name: LDAP User playbook
  vars_files:
    - ./vars/vault.yml
```
eg:
`ansible-playbook playbook.yml --ask-vault-pass`

License
-------

BSD

TODO
----

- Add guacamole support
